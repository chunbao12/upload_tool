# 上传工具

#### 项目介绍
文件上传工具，选择上传，拖住上传，多文件上传

#### 软件架构
软件架构说明


#### 安装教程

引用freeUpload.css
freeUpload.js文件

#### 使用说明

 upload({
        el:'dropzone',  /*元素id*/
        url:'http://sy.cn/home/upload/ups', /*上传地址*/
        type:'POST',/*上传方式*/
        fileName:'file[]',/*上传名称*/
        fileNum:5,/*文件数*/
        fileSize:1024*3,/*文件大小（kb）*/
        fileType:['jpg','gif'],/*文件格式*/
        success:function (data) {
            /*成功*/
            console.log( data );
        },
        error:function (data) {
            /*失败*/
            console.log(data);
        }
    });

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)